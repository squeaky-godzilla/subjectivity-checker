# import SentimentIntensityAnalyzer class
# from vaderSentiment.vaderSentiment module.
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

import argparse

parser = argparse.ArgumentParser(description="Sentiment analysis for english text")
parser.add_argument("--text", type=str, dest="text")

args = parser.parse_args()

source_text = args.text


def sentiment_scores(text):
    compound_threshold_negative = -0.05
    compound_threshold_positive = 0.05

    analyzer = SentimentIntensityAnalyzer()

    score = analyzer.polarity_scores(text)

    if score["compound"] >= compound_threshold_positive:
        sentiment = "positive"

    elif score["compound"] <= compound_threshold_negative:
        sentiment = "negative"

    else:
        sentiment = "neutral"
    return {"sentiment": sentiment, "score": score}


if __name__ == "__main__":

    print(sentiment_scores(source_text))
